<?php require 'vendor/autoload.php';

use Breadoverhead\Images\Process\ImageProcessor;

$settings = require('./settings.php');
$images = $settings['loader']()->loadRecords();
$processor = new ImageProcessor($images, $settings);

$processed_images = $processor->process();
$processed_images_count = count($processed_images);

echo "\n\nProcessed ({$processed_images_count}) Records\n\n";
