<?php require 'vendor/autoload.php';

use Breadoverhead\Images\Process\ImageProcessor;

  $g = false;

  if (isset($_GET['generate'])) {
    $g = 1;
    $settings = require('./settings.php');
    $images = $settings['loader']()->loadRecords();
    $processor = new ImageProcessor($images, $settings);
    $processed_images = $processor->process();
    $processed_images_count = count($processed_images);
  }
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>BREAD OVERHEAD Image Generator</title>
  <meta name="description" content="Mass Generate Meme Content For Social Media">
  <meta name="author" content="@richbreaddy">

  <meta property="og:title" content="Mass Generate Meme Content For Social Media">
  <meta property="og:type" content="website">
  <meta property="og:description" content="Mass Generate Meme Content For Social Media">
  <meta property="og:image" content="image.png">

  <link rel="icon" href="/favicon.ico">
  <link rel="icon" href="/favicon.svg" type="image/svg+xml">
  <link rel="apple-touch-icon" href="/apple-touch-icon.png">

  <link rel="stylesheet" href="css/styles.css?v=1.0">

</head>

<body>
  <?php if ($g): ?>
    <section class="alert-section">
      <div class="alert">
        <div class="alert__message">
          Processed (<?=$processed_images_count; ?>) images ✅
        </div>
      </div>
    </section>
  <?php endif; ?>

  <section class="action-section">
    <a class="action" href="/?generate=1">
      <span class="action__message">
        Generate Images
      </span>
    </a>
  </section>

  <?php if ($g): ?>
    <section class="update-section">
      <?php foreach ($processed_images as $image_update): ?>
        <div class="update-list">
          <h3 class="update-list__heading">
            <?=$image_update->description;?>
          </h3>
          <?php foreach ($image_update->assets as $asset): ?>
            <div class="update-list__item">
              <a href="<?=$asset->path; ?>" target="_blank" class="update-list__action">
                <?=$asset->path; ?>
              </a>
            </div>
          <?php endforeach; ?>
        </div>
      <?php endforeach; ?>
    </section>
  <?php endif; ?>
</body>
</html>
