<?php

use Breadoverhead\Images\Sources\Airtable\AirtableLoader;

return [
  'loader' => function() {
    return new AirtableLoader(
      'keyMXYdNj1f57bzub',
      'appiozGQGEexKrxmQ',
      '1:1%20IG',
      __DIR__."/images"
    );
  },
  'images_relative_path' => 'images',
  'app_dir' => __DIR__,
  'imgix_url' => 'http://breadoverhead.imgix.net',
];
