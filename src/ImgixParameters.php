<?php namespace Breadoverhead\Images;

class ImgixParameters{
  private $params = [];

  private $asset;

  private $url;

  private function appendParam($param_name, $value) {
    $this->params[$this->formatOperationName($param_name)] = $value;
    return $this;
  }

  private static function formatOperationName($param_name) {
    return str_replace('_', '-', $param_name);
  }

  public function __construct($url, $asset) {
    $this->setBaseUrl($url);
    $this->setAsset($asset);
  }

  public function __get($var) {
    if (isset($this->$var)) return $this->$var;

    return $this->params[$var];
  }

  public function __call($param_name, $arguments) {
    return call_user_func_array([$this, 'appendParam'], array_merge([$param_name], $arguments));
  }

  public function __toString() {
    return $this->imgixUri();
  }

  public function setAsset($name) {
    $this->asset = $name;
    return $this;
  }

  public function setBaseUrl($url) {
    $this->url = $url;
  }

  public function centerCrop($hw = 1080) {
    $this->h($hw);
    $this->w($hw);
    $this->fit('crop');
    $this->crop('focalpoint');
    $this->fp_y(.5);
    $this->fp_x(.5);
    return $this;
  }

  public function overlay(ImgixParameters $params, $align) {
    return $this
      ->mark64(base64_encode($params))
      ->mark_align($align);
  }

  public function params() {
    return $this->params;
  }

  public function toQueryString() {
    $query_parts = [];

    foreach ($this->params as $op => $value) {
      if ($value instanceof ImgixParameters) {
        $value = $value->imgixUri();
      }

      $query_parts[]= "{$op}={$value}";
    }

    return implode('&', $query_parts);
  }

  public function imgixUri() {
    return "{$this->url}/{$this->asset}?{$this->toQueryString()}";
  }
}
