<?php namespace Breadoverhead\Images\Process;

use Breadoverhead\Images\ImgixParameters;
use Breadoverhead\Images\TextParameters;

class ImageProcessor {
  public function __construct($images, $settings) {
    $this->images = $images;
    $this->settings = $settings;
  }

  public function process() {
    return array_map(function($record) {
      $img_path = function($filename) {
        return sprintf(
          '%s/%s',
          $this->settings['images_relative_path'],
          $filename
        );
      };

      $processed_images = (object) [
        'description' => $record->text,
        'assets' => []
      ];

      foreach ($record->images as $i => $img) {
        $image_path = sprintf(
          'output/%s-%d.%s',
          $record->output_filename,
          $i,
          substr(strrchr($img, '.'), 1)
        );

        // only continue if file doesn't exist
        // unless the file is zero bytes
        if (file_exists($image_path) && filesize($image_path) > 0) {
          $processed_images->assets[] = (object) [
            'path' => $image_path,
            'abs_path' => ''
          ];
          continue;
        }
        // TODO create mechanism to allow overwrite in some situations


        $imgix_params = new ImgixParameters($this->settings['imgix_url'], $img_path($img));
        $text = TextParameters::make($this->settings['imgix_url'])($record->text);
        $text
          ->txt_font($record->font)
          ->txt_size($record->font_size);

        // get preset class from option
        $preset = "Breadoverhead\\Images\\Presets\\{$record->preset}Preset";
        call_user_func_array([$preset, 'apply'], [$imgix_params, $text, $record]);

        // render the image.
        $file_image = file_get_contents($imgix_params->imgixUri());

        // build file path
        $output_filename = implode('/', [
          $this->settings['app_dir'],
          $image_path
        ]);

        // store rendered image
        file_put_contents($output_filename, $file_image);

        $processed_images->assets[] = (object) [
          'path' => $image_path,
          'abs_path' => $output_filename
        ];
      }

      return $processed_images;
    }, $this->images);
  }
}
