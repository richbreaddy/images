<?php namespace Breadoverhead\Images;

class TextParameters extends ImgixParameters {
  public static function make($url) {
    return function($text, $size=36, $font='impact') use($url) {
      $txt = new static($url, '~text');
      return $txt
        ->txt64(base64_encode($text))
        ->txt_font($font ?: 'impact')
        ->fm('png')
        ->color('fff')
        ->bg(9000)
        ->txt_size($size ?: 36);
    };
  }

  public function color($color) {
    return $this->txt_color($color);
  }

  public function centerAlign($hw=1080, $padding=40) {
    return $this
      ->txt_align('middle,center')
      ->h($hw)
      ->w($hw)
      ->txt_pad($padding);
  }
}
