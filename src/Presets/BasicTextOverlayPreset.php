<?php namespace Breadoverhead\Images\Presets;

use Breadoverhead\Images\ImgixParameters;
use Breadoverhead\Images\TextParameters;

class BasicTextOverlayPreset implements ImgixPresetInterface {
  public static function apply(ImgixParameters $img, TextParameters $text, $options) {
    $text->txt_pad(
      isset($options->text_padding) ? $options->text_padding : 50
    );

    $text->centerAlign();

    $img->overlay(
      $text,
      'center,bottom'
    );

    $img->centerCrop();

    $img->mark_h(
      $img->h
    );
  }
}
