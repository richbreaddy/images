<?php namespace Breadoverhead\Images\Presets;

use Breadoverhead\Images\ImgixParameters;
use Breadoverhead\Images\TextParameters;

interface ImgixPresetInterface {
  public static function apply(ImgixParameters $img, TextParameters $text, $options);
}
