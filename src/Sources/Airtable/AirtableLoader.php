<?php namespace Breadoverhead\Images\Sources\Airtable;

use Breadoverhead\Images\Sources\LoaderInterface;

class AirtableLoader implements LoaderInterface {
  private $api_key, $base_id, $table_id, $img_dir;

  public function __construct($api_key, $base_id, $table_id, $img_dir) {
    $this->api_key  = $api_key;
    $this->base_id  = $base_id;
    $this->table_id = $table_id;
    $this->img_dir  = $img_dir;
  }

  public function loadRecords() {
    return $this->makeRecordsFromResponseData(
      $this->fetchTableData()->records
    );
  }

  private function fetchTableData() {
    return json_decode(
      file_get_contents(
        sprintf(
          'https://api.airtable.com/v0/%s/%s?api_key=%s',
          $this->base_id,
          $this->table_id,
          $this->api_key
        )
      )
    );
  }

  public function makeRecordsFromResponseData(array $records) {
    $img_dir = $this->img_dir;
    return array_map(function($r) use ($img_dir) {
      return new AirtableRecord($r->fields, $img_dir);
    }, $records);
  }
}
