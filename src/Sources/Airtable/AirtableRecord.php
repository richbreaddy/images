<?php namespace Breadoverhead\Images\Sources\Airtable;

use Breadoverhead\Images\Sources\RecordInterface;

class AirtableRecord implements RecordInterface {
  public $images = [];

  public function __construct($data, $img_dir) {
    $this->data = $data;
    $this->storeImages($img_dir);
  }

  public function __get($column_name) {
    return $this->data->$column_name;
  }

  public static function formatFilename($filename) {
    return str_replace(' ', '-', $filename);
  }

  private function storeImages($img_dir) {
    foreach ($this->data->image as $img) {
      $this->images[] = $this->storeImage($img, $img_dir);
    }
  }

  private static function storeImage($img, $img_dir) {
    $filename = static::formatFilename($img->filename);
    $file = "{$img_dir}/{$filename}";
    if (!file_exists($file)) {
      $img_file = file_get_contents($img->url);
      file_put_contents($file, $img_file);
    }
    return $filename;
  }
}
