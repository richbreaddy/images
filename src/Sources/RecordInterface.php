<?php namespace Breadoverhead\Images\Sources;

interface RecordInterface {
  public function __get($column_name);
}
