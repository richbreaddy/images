<?php namespace Breadoverhead\Images\Sources;

interface LoaderInterface {
  public function loadRecords();
}
